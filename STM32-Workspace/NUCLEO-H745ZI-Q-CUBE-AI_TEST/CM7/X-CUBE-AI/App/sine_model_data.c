/**
  ******************************************************************************
  * @file    sine_model_data.c
  * @author  AST Embedded Analytics Research Platform
  * @date    Wed Apr 27 21:24:45 2022
  * @brief   AI Tool Automatic Code Generator for Embedded NN computing
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
#include "sine_model_data.h"
#include "ai_platform_interface.h"


AI_ALIGNED(32)
const ai_u64 s_sine_model_weights_array_u64[ 53 ] = {
  0x42a5810ce1111c75U, 0xdf197861b2d4d5feU, 0xb7700000004U, 0xfffff653U,
  0xc77U, 0x6d300000000U, 0x0U, 0x2172U,
  0xfffff5670000072fU, 0xfffff034U, 0xecef1705ec00fceeU, 0xf5f8fa000103f8e6U,
  0xdbe2def11427ebdcU, 0xf9eee6020631def0U, 0x6e9fffee0071600U, 0x1c9ea181b81efe7U,
  0x1f13ec0ef7da000fU, 0xe0b906fde6b41304U, 0x2605f7ebdef0ec0dU, 0xdf351eea1a6fe41aU,
  0xe11b030f19f1f31aU, 0x181bf6ff19f613deU, 0x1ae5201b1bda1cf0U, 0xdecd01000b96fff5U,
  0xe90efcede316f60dU, 0xe25b021dfc5cebfaU, 0x51300f4ec15f5e1U, 0xf40be70e141d0cecU,
  0xea15022705d70019U, 0xeae8fa0c009b02eaU, 0xeeef020bfd1400fdU, 0x11f7e6060d01ee06U,
  0xf30eff21f1f809f7U, 0x1828e9f21d2612ecU, 0xfb1d1d05f4f3fbe0U, 0x30907e811fc1efdU,
  0xeff91cdcfb36f212U, 0xfd45001d0c6fe7f3U, 0x19e0fa1a190bf00eU, 0xc3beb121c36131fU,
  0x6f1ebfa13e6cbb4U, 0xf40ccbebe518fa1cU, 0x4b500000000U, 0x62d00000a78U,
  0xfffff871U, 0xfffff7fe00000a9aU, 0x9d40000050eU, 0x4b6fffffe47U,
  0xfffff7ac00000000U, 0x54afffff94bU, 0x42ce1d5eca8ba521U, 0x812f54dfb01fce9dU,
  0xffffef8cU,
};



AI_API_DECLARE_BEGIN

/*!
 * @brief Get network weights array pointer as a handle ptr.
 * @ingroup sine_model_data
 * @return a ai_handle pointer to the weights array
 */
AI_DEPRECATED
AI_API_ENTRY
ai_handle ai_sine_model_data_weights_get(void)
{
  static const ai_u8* const s_sine_model_weights_map[1 + 2] = {
    AI_PTR(AI_MAGIC_MARKER),
    AI_PTR(s_sine_model_weights_array_u64),
    AI_PTR(AI_MAGIC_MARKER)
  };

  return AI_HANDLE_PTR(s_sine_model_weights_map);

}


/*!
 * @brief Get network params configuration data structure.
 * @ingroup sine_model_data
 * @return true if a valid configuration is present, false otherwise
 */
AI_API_ENTRY
ai_bool ai_sine_model_data_params_get(ai_handle network, ai_network_params* params)
{
  if (!(network && params)) return false;
  
  static ai_buffer s_sine_model_data_map_activations[AI_SINE_MODEL_DATA_ACTIVATIONS_COUNT] = {
    AI_BUFFER_OBJ_INIT(AI_BUFFER_FORMAT_U8, 1, 1, AI_SINE_MODEL_DATA_ACTIVATIONS_SIZE, 1, NULL)
  };

  const ai_buffer_array map_activations = 
    AI_BUFFER_ARRAY_OBJ_INIT(AI_FLAG_NONE, AI_SINE_MODEL_DATA_ACTIVATIONS_COUNT, s_sine_model_data_map_activations);
  
  static ai_buffer s_sine_model_data_map_weights[AI_SINE_MODEL_DATA_WEIGHTS_COUNT] = {
    AI_BUFFER_OBJ_INIT(AI_BUFFER_FORMAT_U8, 1, 1, 424, 1, s_sine_model_weights_array_u64),
    
  };

  const ai_buffer_array map_weights = 
    AI_BUFFER_ARRAY_OBJ_INIT(AI_FLAG_NONE, AI_SINE_MODEL_DATA_WEIGHTS_COUNT, s_sine_model_data_map_weights);

  return ai_platform_bind_network_params(network, params, &map_weights, &map_activations);
}


AI_API_DECLARE_END
