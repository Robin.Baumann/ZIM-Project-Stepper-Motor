/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "stdio.h"
#include "string.h"
#include "stdlib.h"

#include "TMC4361A.h"
#include "TMC2130.h"

#include "TMCRegisterSettings.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#ifndef HSEM_ID_0
#define HSEM_ID_0 (0U) /* HW semaphore 0*/
#endif

#ifndef HSEM_M4toM7
#define HSEM_M4toM7 (1U) /* HW semaphore protecting read/write M4 to M7 */
#endif

#ifndef HSEM_M4toM7_send
#define HSEM_M4toM7_send (2U) /* HW semaphore signaling that current value was written*/
#endif

#ifndef HSEM_start_measure
#define HSEM_start_measure (3U) /* HW semaphore signaling that encoder should be counted*/
#endif

#ifndef HSEM_stop_measure
#define HSEM_stop_measure (4U) /* HW semaphore signaling that encoder should stop being counted*/
#endif

#define SRAM_BUFF_SIZE (1)

#define MICROSTEPS (256)
#define SHIFT_VEL(x) ((x) << 8)

#define START_VEL (MICROSTEPS*20) 				// 20steps/s 	-> 0.1rps
#define MIN_VEL (MICROSTEPS*100) 				// 100steps/s 	-> 0.5rps

#define MAX_ACC (TMC4361A_MAX_ACCELERATION) 	// max acceleration/deceleration is max possible for controller

#define MAX_REPS (99) 							// max repetitions to loop through
#define SMALL_REPS (3) 							// max repetitions for small movement part
#define MAX_DISTANCE (9800*MICROSTEPS) 			// max Distnace of linear stage

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

CRC_HandleTypeDef hcrc;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim16;

/* Definitions for DefaultTask */
osThreadId_t DefaultTaskHandle;
const osThreadAttr_t DefaultTask_attributes = {
  .name = "DefaultTask",
  .stack_size = 512 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* USER CODE BEGIN PV */

// inter-core buffers
struct shared_data
{
	 int32_t M4toM7[SRAM_BUFF_SIZE];
};

// pointer to shared_data struct (inter-core buffers and status)
volatile struct shared_data __attribute__((section(".DATA_RAM_D3")))sram_mem;

// counter for motion patterns
volatile uint8_t ramp_pattern = 0;
volatile uint16_t motion_pattern = 0;
volatile uint16_t vel_pattern_1 = 1;
volatile uint16_t vel_pattern_2 = 1;
volatile uint16_t vel_pattern_3 = 1;

// flag to let main task know it should start to move the motor / stop the timer and encoder interupt
volatile uint8_t start_motor = 0;
volatile uint8_t stop_motor = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
static void MX_GPIO_Init(void);
static void MX_CRC_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM16_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM6_Init(void);
void StartDefaultTask(void *argument);

/* USER CODE BEGIN PFP */

void writeController(uint8_t adress, uint32_t datagram);
void writeDriver(uint8_t adress, uint32_t datagram);

int32_t readController(uint8_t adress);
int32_t readDriver(uint8_t address);

void setDefaultRegisterStateController(void);
void setDefaultRegisterStateDriver(void);
void resetControllerDriver(void);

uint8_t MoveMotor(void);

void GetControllerData(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if(GPIO_Pin == BLUE_BUTTON_Pin)
    {
		// set flag to start motor
		start_motor = 1;
    }
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

/* USER CODE BEGIN Boot_Mode_Sequence_1 */
  /*HW semaphore Clock enable*/
  __HAL_RCC_HSEM_CLK_ENABLE();
  /* Activate HSEM notification for Cortex-M4*/
  HAL_HSEM_ActivateNotification(__HAL_HSEM_SEMID_TO_MASK(HSEM_ID_0));
  /*
  Domain D2 goes to STOP mode (Cortex-M4 in deep-sleep) waiting for Cortex-M7 to
  perform system initialization (system clock config, external memory configuration.. )
  */
  HAL_PWREx_ClearPendingEvent();
  HAL_PWREx_EnterSTOPMode(PWR_MAINREGULATOR_ON, PWR_STOPENTRY_WFE, PWR_D2_DOMAIN);
  /* Clear HSEM flag */
  __HAL_HSEM_CLEAR_FLAG(__HAL_HSEM_SEMID_TO_MASK(HSEM_ID_0));

/* USER CODE END Boot_Mode_Sequence_1 */
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CRC_Init();
  MX_SPI1_Init();
  MX_TIM16_Init();
  MX_DMA_Init();
  MX_TIM6_Init();
  /* USER CODE BEGIN 2 */

    /* Start PWM pin (CLK16) */
  	HAL_TIM_PWM_Start(&htim16, TIM_CHANNEL_1);

  	/* Enable SPI */
  	__HAL_SPI_ENABLE(&hspi1);

  	/* Reset controller and driver and write default register values */
  	resetControllerDriver();


  	// wait a second after setup
  	HAL_Delay(1000);

  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of DefaultTask */
  DefaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &DefaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief CRC Initialization Function
  * @param None
  * @retval None
  */
static void MX_CRC_Init(void)
{

  /* USER CODE BEGIN CRC_Init 0 */

  /* USER CODE END CRC_Init 0 */

  /* USER CODE BEGIN CRC_Init 1 */

  /* USER CODE END CRC_Init 1 */
  hcrc.Instance = CRC;
  hcrc.Init.DefaultPolynomialUse = DEFAULT_POLYNOMIAL_ENABLE;
  hcrc.Init.DefaultInitValueUse = DEFAULT_INIT_VALUE_ENABLE;
  hcrc.Init.InputDataInversionMode = CRC_INPUTDATA_INVERSION_NONE;
  hcrc.Init.OutputDataInversionMode = CRC_OUTPUTDATA_INVERSION_DISABLE;
  hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_BYTES;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CRC_Init 2 */

  /* USER CODE END CRC_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi1.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 0x0;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  hspi1.Init.NSSPolarity = SPI_NSS_POLARITY_LOW;
  hspi1.Init.FifoThreshold = SPI_FIFO_THRESHOLD_01DATA;
  hspi1.Init.TxCRCInitializationPattern = SPI_CRC_INITIALIZATION_ALL_ZERO_PATTERN;
  hspi1.Init.RxCRCInitializationPattern = SPI_CRC_INITIALIZATION_ALL_ZERO_PATTERN;
  hspi1.Init.MasterSSIdleness = SPI_MASTER_SS_IDLENESS_00CYCLE;
  hspi1.Init.MasterInterDataIdleness = SPI_MASTER_INTERDATA_IDLENESS_00CYCLE;
  hspi1.Init.MasterReceiverAutoSusp = SPI_MASTER_RX_AUTOSUSP_DISABLE;
  hspi1.Init.MasterKeepIOState = SPI_MASTER_KEEP_IO_STATE_DISABLE;
  hspi1.Init.IOSwap = SPI_IO_SWAP_DISABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 240-1;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 1000-1;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief TIM16 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM16_Init(void)
{

  /* USER CODE BEGIN TIM16_Init 0 */

  /* USER CODE END TIM16_Init 0 */

  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM16_Init 1 */

  /* USER CODE END TIM16_Init 1 */
  htim16.Instance = TIM16;
  htim16.Init.Prescaler = 0;
  htim16.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim16.Init.Period = 20-1;
  htim16.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim16.Init.RepetitionCounter = 0;
  htim16.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim16) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim16) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 15;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim16, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim16, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM16_Init 2 */

  /* USER CODE END TIM16_Init 2 */
  HAL_TIM_MspPostInit(&htim16);

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(NFREEZE_GPIO_Port, NFREEZE_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, DRV_EN_Pin|LED_YELLOW_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(USB_OTG_FS_PWR_EN_GPIO_Port, USB_OTG_FS_PWR_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : BLUE_BUTTON_Pin */
  GPIO_InitStruct.Pin = BLUE_BUTTON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BLUE_BUTTON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : SPI_CS_Pin */
  GPIO_InitStruct.Pin = SPI_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(SPI_CS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : NFREEZE_Pin DRV_EN_Pin LED_YELLOW_Pin */
  GPIO_InitStruct.Pin = NFREEZE_Pin|DRV_EN_Pin|LED_YELLOW_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : LD3_Pin */
  GPIO_InitStruct.Pin = LD3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD3_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_OTG_FS_PWR_EN_Pin */
  GPIO_InitStruct.Pin = USB_OTG_FS_PWR_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(USB_OTG_FS_PWR_EN_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */

void writeController(uint8_t address, uint32_t datagram)
{
	// buffer variables
	uint8_t t_address = address | TMC4361A_WRITE_BIT;
	uint8_t t_data1 = (datagram >> 24) & 0xff;
	uint8_t t_data2 = (datagram >> 16) & 0xff;
	uint8_t t_data3 = (datagram >> 8) & 0xff;
	uint8_t t_data4 = (datagram) & 0xff;

	// send data
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &t_address, 1, 10);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &t_data1, 1, 10);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &t_data2, 1, 10);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &t_data3, 1, 10);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &t_data4, 1, 10);
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);
}

void writeDriver(uint8_t address, uint32_t datagram)
{
  writeController(TMC4361A_COVER_HIGH_WR, address | TMC2130_WRITE_BIT);
  writeController(TMC4361A_COVER_LOW_WR, datagram);
}

int32_t readController(uint8_t address)
{
	int value;
	uint8_t data[5] = {0};
	uint8_t r_data[5];

	// seet MSB to be address
	data[0] = TMC_ADDRESS(address);

	// send data to signal read access
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &data[0], 1, 10);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &data[1], 1, 10);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &data[2], 1, 10);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &data[3], 1, 10);
	HAL_SPI_Transmit(&hspi1, (uint8_t*) &data[4], 1, 10);
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);

	// read data send back after read acces has been signaled
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Receive(&hspi1, (uint8_t*) &r_data[0], 1, 10);
	HAL_SPI_Receive(&hspi1, (uint8_t*) &r_data[1], 1, 10);
	HAL_SPI_Receive(&hspi1, (uint8_t*) &r_data[2], 1, 10);
	HAL_SPI_Receive(&hspi1, (uint8_t*) &r_data[3], 1, 10);
	HAL_SPI_Receive(&hspi1, (uint8_t*) &r_data[4], 1, 10);
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);

	// Build Data from bits
	value = (((uint32_t)r_data[1]) << 24);
	value |= (((uint32_t)r_data[2]) << 16);
	value |= ((r_data[3]) << 8);
	value |= (r_data[4]);

	return value;
}

int32_t readDriver(uint8_t address)
{
	uint64_t value = 0;

	// write datagrams signaling
	writeController(TMC4361A_COVER_HIGH_WR, TMC_ADDRESS(address));
	writeController(TMC4361A_COVER_LOW_WR, 0);

	// Trigger a re-send by writing the low register again
	writeController(TMC4361A_COVER_LOW_WR, 0);

	// Read the reply
	value |= (uint64_t) readController(TMC4361A_COVER_DRV_HIGH_RD) << 32;
	value |= readController(TMC4361A_COVER_DRV_LOW_RD);

	return value;
}

void setDefaultRegisterStateController(void)
{
	for(uint8_t i=0; i<TMC4361A_REGISTER_COUNT; i++)
	{
		if(tmc4361A_defaultRegisterAccess[i] != 0x00 && tmc4361A_defaultRegisterAccess[i] != 0x01 && tmc4361A_defaultRegisterAccess[i] != 0x13)
			writeController(i, TMC4361A_defaultRegisterSetting[i]);
	}
}

void setDefaultRegisterStateDriver(void)
{
	for(uint8_t i=0; i<TMC2130_REGISTER_COUNT; i++)
	{
		if(tmc2130_defaultRegisterAccess[i] != 0x00 && tmc2130_defaultRegisterAccess[i] != 0x01 && tmc2130_defaultRegisterAccess[i] != 0x21)
			writeDriver(i, TMC2130_defaultRegisterSetting[i]);
	}
}

void resetControllerDriver(void)
{
	// reset driver and controller and set default register values
  	writeController(TMC4361A_RESET_REG,0x52535400);
  	setDefaultRegisterStateController();
  	setDefaultRegisterStateDriver();
}

uint8_t MoveMotor(void)
{
	if(motion_pattern == 0)
	{
		writeController(TMC4361A_VMAX, SHIFT_VEL(START_VEL));

		writeController(TMC4361A_XACTUAL, 0);					// reset position
		writeController(TMC4361A_X_TARGET, (MICROSTEPS*100));  	// 100 steps

		motion_pattern++;

		return 1;
	}
	else if(motion_pattern == 1)
	{
		writeController(TMC4361A_VMAX, SHIFT_VEL(MIN_VEL*vel_pattern_1));

		writeController(TMC4361A_XACTUAL, 0);	// reset position
		writeController(TMC4361A_X_TARGET, MAX_DISTANCE);

		motion_pattern++;

		return 1;
	}
	else if(motion_pattern == 2)
	{
		writeController(TMC4361A_X_TARGET, 0);	// steps back

		motion_pattern++;
		vel_pattern_1++;

		if(vel_pattern_1 <= (MAX_REPS/(SMALL_REPS*(ramp_pattern+1)))) motion_pattern = 1;

		return 1;
	}
	else if(motion_pattern == 3)
	{
		if(((vel_pattern_2-1) % (SMALL_REPS*(ramp_pattern+1))) == 0)
			writeController(TMC4361A_VMAX, SHIFT_VEL(MIN_VEL*(vel_pattern_2-1)/(SMALL_REPS*(ramp_pattern+1))));

		writeController(TMC4361A_XACTUAL, 0); 					// reset position
		writeController(TMC4361A_X_TARGET, (MICROSTEPS*120));  	// 120 steps

		motion_pattern++;

		return 1;
	}
	else if(motion_pattern == 4)
	{
		writeController(TMC4361A_X_TARGET, (MICROSTEPS*100));	// 20 steps back

		motion_pattern++;
		vel_pattern_2++;

		if(vel_pattern_2 <= MAX_REPS) motion_pattern = 3;

		return 1;
	}
	else if(motion_pattern == 5)
	{
		if(((vel_pattern_3-1) % (SMALL_REPS*(ramp_pattern+1))) == 0)
			writeController(TMC4361A_VMAX, SHIFT_VEL(MIN_VEL*(vel_pattern_3-1)/(SMALL_REPS*(ramp_pattern+1))));

		writeController(TMC4361A_XACTUAL, (MICROSTEPS*120));	// reset position
		writeController(TMC4361A_X_TARGET, 0);  			 	// 120 steps back

		motion_pattern++;

		return 1;
	}
	else if(motion_pattern == 6)
	{
		writeController(TMC4361A_X_TARGET, (MICROSTEPS*20));	// 20 steps

		motion_pattern++;
		vel_pattern_3++;

		if(vel_pattern_3 <= MAX_REPS) motion_pattern = 5;

		return 1;
	}
	else if(motion_pattern == 7)
	{
		writeController(TMC4361A_VMAX, SHIFT_VEL(START_VEL));

		writeController(TMC4361A_XACTUAL, (MICROSTEPS*100));	// reset position
		writeController(TMC4361A_X_TARGET, 0);					// 100 steps back

		motion_pattern++;

		return 1;
	}
	else
	{
		return 0;
	}
}

void GetControllerData(void)
{
	// write message to shared SRAM (protect by HSEM as saftey)
	while(HAL_HSEM_FastTake(HSEM_M4toM7) != HAL_OK);
	sram_mem.M4toM7[0] = readController(TMC4361A_VACTUAL);
	HAL_HSEM_Release(HSEM_M4toM7,0);

	// Notify M7 that data was written
	HAL_HSEM_FastTake(HSEM_M4toM7_send);
	HAL_HSEM_Release(HSEM_M4toM7_send,0);

	// motor stands still -> next pattern
	if(((readDriver(TMC2130_DRV_STATUS)&TMC2130_STST_MASK)>>TMC2130_STST_SHIFT) && (motion_pattern > 0))
	{
		// execute next movement pattern or set stop flag if all patterns have been executed
		if(!MoveMotor()) stop_motor = 1;
	}
}

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the DefaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {
	if(start_motor == 1)
	{
		// set ramp mode
		writeController(TMC4361A_RAMPMODE, 5-ramp_pattern);
		writeController(TMC4361A_VBREAK, 0);
		writeController(TMC4361A_VSTOP, 0);
		writeController(TMC4361A_AMAX, MAX_ACC);
		writeController(TMC4361A_DMAX, MAX_ACC);
		writeController(TMC4361A_VSTART, SHIFT_VEL(START_VEL));

		// Notify M7 to start Encoder counter
		HAL_HSEM_FastTake(HSEM_start_measure);
		HAL_HSEM_Release(HSEM_start_measure,0);

    	// Start Timer interrupt
		HAL_TIM_Base_Start_IT(&htim6);

		// wait a few seconds to ensure timer and encoder capture all movement data and to get more stand still data in the beginning
		osDelay(5000);

		// start to move motor with first pattern
		MoveMotor();

//		writeController(TMC4361A_VMAX, SHIFT_VEL(MICROSTEPS*200*5));
//		writeController(TMC4361A_VSTART, START_VEL);
//		writeController(TMC4361A_XACTUAL, (MICROSTEPS*200));
//		writeController(TMC4361A_X_TARGET, 0);

		// reset flag
		start_motor = 0;
	}

	if(stop_motor == 1)
	{
		// wait a few seconds after motor stopped to stop timer in order to get more stand still data at the end
		osDelay(5000);

		// stop timer to get data
		HAL_TIM_Base_Stop_IT(&htim6);

		// Notify M7 to stop measuring
		HAL_HSEM_FastTake(HSEM_stop_measure);
		HAL_HSEM_Release(HSEM_stop_measure,0);

		// reset all registers
		resetControllerDriver();

		// reset flags and motion pattern
		stop_motor = 0;
		motion_pattern = 0;
		vel_pattern_1 = 1;
		vel_pattern_2 = 1;
		vel_pattern_3 = 1;

		if(ramp_pattern == 0)	ramp_pattern++;
		else					ramp_pattern = 0;

	}
  }
  /* USER CODE END 5 */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM14 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  // Check which version of the timer triggered this callback and call Meassure current Task
  if (htim->Instance == TIM6)
  {
	  GetControllerData();
  }

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM14) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
